# FocusLock-Revamped

We are currently revamping FocusLock's website!
<br>
We are now using Bulma and FontAwesome.

## Current in-progress pages
&bull; index

## Current revamped pages
&bull; Profile

## Todo revamp
&bull; about <br>
&bull; checkout <br>
&bull; contact <br>
&bull; copyright <br>
&bull; login <br>
&bull; privacy policy <br>
&bull; process <br>
&bull; signup <br>
&bull; signup success <br>
&bull; support <br>
&bull; error paths <br>
&bull; (admin) admin panel <br>
&bull; (admin) email <br>
&bull; (admin) lockpick <br>
&bull; (admin) analytics <br>
&bull; (admin) backdoor <br>
