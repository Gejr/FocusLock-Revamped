$(document).ready(function() {
	var navBarTop = $('#nav_bar').offset().top;
	var stickyNav = function() {
		var scrollTop = $(window).scrollTop();

		if(scrollTop > navBarTop) {
			$('#nav_bar').addClass('navbar-fixed');
		} else {
			$('#nav_bar').removeClass('navbar-fixed');
		}
	};
	stickyNav();
	$(window).scroll(function() {
		stickyNav();
	});

  	$(".navbar-burger").click(function() {
      	$(".navbar-burger").toggleClass("is-active");
      	$(".navbar-menu").toggleClass("is-active");
  	});

	$(".li-tabs-1").click(function() {
		hideAll();
		$(".li-tabs-1").addClass("is-active");
		$(".li-tabs-2").removeClass("is-active");
  		$(".li-tabs-3").removeClass("is-active");
		$(".li-tabs-1-content").removeClass("hidden");
  	});
	$(".li-tabs-2").click(function() {
		hideAll();
		$(".li-tabs-1").removeClass("is-active");
		$(".li-tabs-2").addClass("is-active");
  		$(".li-tabs-3").removeClass("is-active");
		$(".li-tabs-2-content").removeClass("hidden");
  	});
	$(".li-tabs-3").click(function() {
		hideAll();
		$(".li-tabs-1").removeClass("is-active");
		$(".li-tabs-2").removeClass("is-active");
  		$(".li-tabs-3").addClass("is-active");
		$(".li-tabs-3-content").removeClass("hidden");
  	});

	function hideAll(){
      $(".li-tabs-1-content").addClass("hidden");
      $(".li-tabs-2-content").addClass("hidden");
      $(".li-tabs-3-content").addClass("hidden");
    }
});
